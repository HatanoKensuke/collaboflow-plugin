jQuery.noConflict();

(function($, PLUGIN_ID) {
    'use strict';

    const pluginSettings = $("#collaboflow-plugin-settings");
    const rootUrl = $("#collaboflow-root-url");
    const instanceCodeField = $("#collaboflow-instanceCode-field");
    const documentButtonLabel = $("#collaboflow-documentbutton-label");

    // 文書IDフィールド選択用プルダウンリスト作成
    kintone.api('/k/v1/app/form/fields', 'GET',{
        app: kintone.app.getId()
    },).then(function(res) {
        // 取得情報を元にプルダウンリスト作成
        for (let key in res.properties) {
            if (!res.properties.hasOwnProperty(key)) {continue;}
            let prop = res.properties[key];
            if (prop.type === 'SINGLE_LINE_TEXT') {
                instanceCodeField.append(
                    $('<option>').val(prop.code).text(prop.label)
                );
            }
        }
    }, function(error) {
        // エラーの場合はメッセージ表示
        let errmsg = 'kintoneフィールド情報取得中にエラーが発生しました。';
        if (error.message !== undefined) {
            errmsg += '\n' + error.message;
        }
        alert(errmsg);
    });

    function getSettingsUrl() {
        return '/k/admin/app/flow?app=' + kintone.app.getId();
    }

    // 設定値の取得
    const config = kintone.plugin.app.getConfig(PLUGIN_ID);
    if (config.rootUrl) {
        rootUrl.val(config.rootUrl);
    }
    if (config.instanceCodeField) {
        instanceCodeField.val(config.instanceCodeField);
    }
    if (config.documentButtonLabel) {
        documentButtonLabel.val(config.documentButtonLabel);
    }else{
        documentButtonLabel.val("申請書を開く");
    }

    // 設定値の保存
    pluginSettings.on('submit', function(e) {
        e.preventDefault();
        kintone.plugin.app.setConfig({
            "rootUrl": rootUrl.val(),
            "instanceCodeField": instanceCodeField.val(),
            "documentButtonLabel": documentButtonLabel.val()
            }, function() {
            alert('設定を保存します。有効にするには、アプリを更新してください。');
            window.location.href = getSettingsUrl();
        });
    });
    
})(jQuery, kintone.$PLUGIN_ID);
