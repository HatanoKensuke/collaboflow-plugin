jQuery.noConflict();

(function($, PLUGIN_ID) {
    'use strict';

    const config = kintone.plugin.app.getConfig(PLUGIN_ID);
    const collaboflowUrl = config.rootUrl;
    const instanceCodeField = config.instanceCodeField;
    const documentButtonLabel = config.documentButtonLabel;

    kintone.events.on('app.record.detail.show', function(event) {

        // 画面上部にボタンを配置
        let collaboflowBtn = document.createElement('button');
        collaboflowBtn.id = 'collaboflow-document-open';
        collaboflowBtn.classList.add("kintoneplugin-button-normal");
        collaboflowBtn.innerHTML = documentButtonLabel;
        collaboflowBtn.onclick = function(){
            openCollaboflowDocument();
        }
        kintone.app.record.getHeaderMenuSpaceElement().appendChild(collaboflowBtn);

    });

    // コラボフロー文書を開く
    function openCollaboflowDocument(){
        const currentRecord = kintone.app.record.get();

        // 文書IDフィールドの存在チェック
        if(!currentRecord.record[instanceCodeField]){
            alert("コラボフロー文書IDフィールド[" + instanceCodeField + "]が見つかりません");
            return false;
        }
        // コラボフロー申請文書を開く
        let instanceCode = parseInt(currentRecord.record[instanceCodeField].value);
        const windowAttributes = "width=400, height=300, resizable=yes, menubar=no, toolbar=no, scrollbars=yes, status=yes, top=20, left=20 ";
        const windowUrl = collaboflowUrl + "&fuseaction=clz_DocumentDesc&nPInstanceCD=" + instanceCode;
        window.open(windowUrl,'winDoc',windowAttributes);

    };

})(jQuery, kintone.$PLUGIN_ID);
